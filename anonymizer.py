# -*- coding: utf8 -*-
import ConfigParser
import random
import urllib

from flask import Flask, request
from MozillaEmulator import *

config = ConfigParser.RawConfigParser()
config.read('settings.ini')
answers = [u'ап']
app = Flask(__name__)
dl = MozillaEmulator()


@app.route('/')
def home():
    url = "http://%s/" % config.get('account', 'DOMAIN')
    return dl.download(url)


@app.route('/<path:link>', methods=['GET', 'POST'])
def index(link):
    url = "http://%s/%s?%s&lol=%s" % (config.get('account', 'DOMAIN'), link, 
        request.query_string, random.random())
    if request.method == 'POST':
        answer = request.form.to_dict()['LevelAction.Answer']
        global answers
        if not answer in answers:
            answers.append(answer)
            return dl.download(url, postdata=urllib.urlencode({k:v.encode('utf-8') for k, v in request.form.to_dict().items()}))
        else:
            if answer == u'ап':
                answers = [u'ап']
    return dl.download(url)


if __name__ == "__main__":
    login_url = "http://%s/Login.aspx?return=/" % config.get('account', 'DOMAIN')
    dl.download(login_url, postdata=urllib.urlencode({
        "Login": config.get('account', 'USERNAME'), 
        "Password": config.get('account', 'PASSWORD')}))
    app.run(port=2678, host='0.0.0.0')
