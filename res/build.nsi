!define APPNAME "Anonymizer"
!define COMPANYNAME "Anonymous"
!define DESCRIPTION "A simple anonymizer for Encounter games(en.cx)"
!define VERSIONMAJOR 1
!define VERSIONMINOR 1
!define VERSIONBUILD 1
!define HELPURL "" # "Support Information" link
!define UPDATEURL "" # "Product Updates" link
!define ABOUTURL "" # "Publisher" link
!define INSTALLSIZE 3
 
InstallDir "$PROGRAMFILES\${COMPANYNAME}\${APPNAME}"
 
LicenseData "license.rtf"
Name "${COMPANYNAME} - ${APPNAME}"
Icon "logo.ico"
outFile "anonymizer-installer.exe"
 
!include LogicLib.nsh
 
page license
page directory
Page instfiles
 
section "install"
	setOutPath $INSTDIR
	file "anonymizer.exe"
  file "bz2.so"
  file "_ctypes.so"
  file "datetime.so"
  file "_heapq.so"
  file "_io.so"
  file "_json.so"
  file "libpython2.7.so.1.0"
  file "library.zip"
  file "markupsafe._speedups.so"
  file "_multibytecodec.so"
  file "OpenSSL.crypto.so"
  file "OpenSSL.rand.so"
  file "OpenSSL.SSL.so"
  file "pyexpat.so"
  file "readline.so"
  file "settings.ini"
  file "simplejson._speedups.so"
  file "termios.so"
	file "logo.ico"
  file "license.rtf"
 
	writeUninstaller "$INSTDIR\uninstall.exe"
 
	createDirectory "$SMPROGRAMS\${COMPANYNAME}"
	createShortCut "$SMPROGRAMS\${COMPANYNAME}\${APPNAME}.lnk" "$INSTDIR\anonymizer.exe" "" "$INSTDIR\logo.ico"
  createShortCut "$SMPROGRAMS\${COMPANYNAME}\settings.ini" "$INSTDIR\settings.ini" ""
 
	# Registry information for add/remove programs
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "DisplayName" "${COMPANYNAME} - ${APPNAME} - ${DESCRIPTION}"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "UninstallString" "$\"$INSTDIR\uninstall.exe$\""
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "QuietUninstallString" "$\"$INSTDIR\uninstall.exe$\" /S"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "InstallLocation" "$\"$INSTDIR$\""
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "DisplayIcon" "$\"$INSTDIR\logo.ico$\""
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "Publisher" "$\"${COMPANYNAME}$\""
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "HelpLink" "$\"${HELPURL}$\""
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "URLUpdateInfo" "$\"${UPDATEURL}$\""
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "URLInfoAbout" "$\"${ABOUTURL}$\""
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "DisplayVersion" "$\"${VERSIONMAJOR}.${VERSIONMINOR}.${VERSIONBUILD}$\""
	WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "VersionMajor" ${VERSIONMAJOR}
	WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "VersionMinor" ${VERSIONMINOR}
	WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "NoModify" 1
	WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "NoRepair" 1
	WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "EstimatedSize" ${INSTALLSIZE}
sectionEnd
 
# Uninstaller
 
section "uninstall"
 
	# Remove Start Menu launcher
	delete "$SMPROGRAMS\${COMPANYNAME}\${APPNAME}.lnk"
  delete "$SMPROGRAMS\${COMPANYNAME}\settings.ini"
	# Try to remove the Start Menu folder - this will only happen if it is empty
	rmDir "$SMPROGRAMS\${COMPANYNAME}"
 
	# Remove files
  delete $INSTDIR\anonymizer.exe
  delete $INSTDIR\bz2.so
  delete $INSTDIR\_ctypes.so
  delete $INSTDIR\datetime.so
  delete $INSTDIR\_heapq.so
  delete $INSTDIR\_io.so
  delete $INSTDIR\_json.so
  delete $INSTDIR\libpython2.7.so.1.0
  delete $INSTDIR\library.zip
  delete $INSTDIR\settings.ini
  delete $INSTDIR\markupsafe._speedups.so
  delete $INSTDIR\_multibytecodec.so
  delete $INSTDIR\OpenSSL.crypto.so
  delete $INSTDIR\OpenSSL.rand.so
  delete $INSTDIR\OpenSSL.SSL.so
  delete $INSTDIR\pyexpat.so
  delete $INSTDIR\readline.so
  delete $INSTDIR\simplejson._speedups.so
  delete $INSTDIR\termios.so
	delete $INSTDIR\logo.ico
  delete $INSTDIR\license.rtf

 
	# Always delete uninstaller as the last action
	delete $INSTDIR\uninstall.exe
 
	# Try to remove the install directory - this will only happen if it is empty
	rmDir $INSTDIR
 
	# Remove uninstaller information from the registry
	DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}"
sectionEnd
